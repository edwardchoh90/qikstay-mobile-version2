This is a preview alpha version for [QikStayIonic]

## Installation requirement (for apk)
1.) Install NodeJS and NPM (Node Package Manager
2.) Install Android Studio with Java 8 (for compiling). 
3.) Install Android version 22 on Android SDK 


### Installing Ionic
1.) Go to command prompt/Terminal and type npm install -g ionic@latest

### Preparing the file for deploy
1.) cd to the project directory and run npm install

### Live Preview source code on Browser
1.) type ionic serve --lab

### Compiling apk
1.) run this command ionic cordova build android --prod
2.) The apk file will be located at /platform/build/outputs/android-debug.apk


Take the name after `ionic2-starter-`, and that is the name of the template to be used when using the `ionic start` command below:

```bash
$ sudo npm install -g ionic cordova
$ ionic start mySideMenu sidemenu
```

Then, to run it, cd into `mySideMenu` and run:

```bash
$ ionic cordova platform add ios
$ ionic cordova run ios
```

Substitute ios for android if not on a Mac.

