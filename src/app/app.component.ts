import { Component, ViewChild } from '@angular/core';
import {AlertController, Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';


import { RoomBookPage} from "../pages/room-book/room-book";
import { PromotionPage } from "../pages/promotion/promotion";
import { RoomPromotionPage } from "../pages/promotion/room-promotion/room-promotion";
import { FnbPromotionPage } from "../pages/promotion/fnb-promotion/fnb-promotion";
import { RoomDetailPage } from "../pages/room-detail/room-detail";
import { PromotionTabsPage } from "../pages/promotion-tabs/promotion-tabs";
import { EventsPage } from "../pages/events/events";
import { RegisterPage } from "../pages/register/register";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = RoomBookPage;
  companyName = "Bay View Hotel"
  pages: Array<{title: string, component: any ,icon: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public altCtrl: AlertController,
              public splashScreen: SplashScreen, private push: Push) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Book Room', component: RoomBookPage, icon: "md-card"},
      { title: 'Events', component: EventsPage, icon: "md-calendar"},
      { title: 'Promotion', component: PromotionTabsPage, icon: "md-cafe"},
      { title: 'Room Detail', component: RoomDetailPage, icon: "md-person"},
      { title: 'Register', component: RegisterPage, icon:"ios-create"}
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.initPushNotification();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  initPushNotification() {

    if (this.platform.is('core') || this.platform.is('mobileweb')) {

    } else {

      this.push.hasPermission()
        .then((res: any) => {
          if (res.isEnabled) {
            console.log('We have permission to send push notifications');
          } else {
            console.log('We don\'t have permission to send push notifications');
          }
        });

      const options: PushOptions = {
        android: {
          sound: true,
          vibrate: true,

        },
        ios: {
          alert: 'true',
          badge: true,
          sound: 'false'
        },
        windows: {}
      };
      const pushObject: PushObject = this.push.init(options);
      pushObject.on('notification').subscribe((notification: any) => {
        console.log('Received a notification', notification);

        let confirmAlert = this.altCtrl.create({
          title: 'New Notification',
          message: JSON.stringify(notification),
          buttons: [{
            text: 'Ignore',
            role: 'cancel'
          }, {
            text: 'View',
            handler: () => {

            }
          }]
        });
        confirmAlert.present();

      });

      pushObject.on('registration').subscribe((registration: any) =>
        console.log('Device registered', registration));
      pushObject.on('error').subscribe(error =>
        console.error('Error with Push plugin', error));


    }
  }

}
