import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LocalNotifications} from "@ionic-native/local-notifications";
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { DatePicker } from "@ionic-native/date-picker";
import { ViewMoreComponent } from "../components/view-more/view-more";

import { RoomBookPage} from "../pages/room-book/room-book";
import { PromotionPage } from "../pages/promotion/promotion";
import { RoomPromotionPage} from "../pages/promotion/room-promotion/room-promotion";
import { FnbPromotionPage} from "../pages/promotion/fnb-promotion/fnb-promotion";
import { HotelPromotionProvider } from '../providers/hotel-promotion/hotel-promotion';
import { RoomDetailPage } from "../pages/room-detail/room-detail";
import { RoomInfoProvider } from '../providers/room-info/room-info';
import { PromotionTabsPage } from "../pages/promotion-tabs/promotion-tabs";
import { EventsPage } from "../pages/events/events";
import { RegisterPage } from "../pages/register/register";
import { EventsProvider } from '../providers/events/events';
import { SearchEvent } from "../pages/events/eventSearch/searchEvent";
import { RoomDetailProvider } from '../providers/room-detail/room-detail';

@NgModule({
  declarations: [
    MyApp,
    RoomBookPage,
    PromotionPage,
    RoomPromotionPage,
    FnbPromotionPage,
    RoomDetailPage,
    PromotionTabsPage,
    ViewMoreComponent,
    EventsPage,
    RegisterPage,
    SearchEvent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{tabsPlacement: 'top'}),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RoomBookPage,
    PromotionPage,
    RoomPromotionPage,
    FnbPromotionPage,
    RoomDetailPage,
    PromotionTabsPage,
    ViewMoreComponent,
    EventsPage,
    RegisterPage,
    SearchEvent,
  ],
  providers: [
    StatusBar,
    DatePicker,
    LocalNotifications,
    SplashScreen,
    Push,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HotelPromotionProvider,
    RoomInfoProvider,
    EventsProvider,
    RoomDetailProvider
  ]
})
export class AppModule {}
