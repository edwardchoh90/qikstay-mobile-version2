import { Component } from '@angular/core';
import { NavParams, ViewController} from "ionic-angular";

/**
 * Generated class for the ViewMoreComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'view-more',
  templateUrl: 'view-more.html'
})

export class ViewMoreComponent {

  eventDetail;
  fnbDetail
  roomDetail;
  roomPromoDetail;


  detail = {
    image:"",
    title:"",
    info1:"",
    info2:"",
    info3:"",
    info4:"",
    details:"",
    titleText:"",
    occupancy:[],
  }

  constructor(public view:ViewController, public navParams: NavParams) {


    console.log('Hello ViewMoreComponent Component');
    this.eventDetail = this.navParams.get("eventDetails")
    this.fnbDetail = this.navParams.get('fnbDetail')
    this.roomDetail = this.navParams.get('roomDetail')
    this.roomPromoDetail = this.navParams.get('roomPromoDetail')

    // REMAPPING DETAILS TO FIT THE TEMPLATE

    if (this.eventDetail != null){

      this.detail.image = this.eventDetail.image;
      this.detail.title = this.eventDetail.eventName;
      this.detail.info1 = this.eventDetail.date;
      this.detail.info2 = this.eventDetail.venue;
      this.detail.details = this.eventDetail.details
      this.detail.titleText = "Event Detail"

    }

    console.log(this.fnbDetail)

    if (this.fnbDetail != null){

      this.detail.image = this.fnbDetail.image;
      this.detail.title = this.fnbDetail.promoName;
      this.detail.info1 = this.fnbDetail.date;
      this.detail.details = this.fnbDetail.details
      this.detail.titleText = "Promotion Detail"

    }

    if (this.roomPromoDetail != null){

      this.detail.image = this.roomPromoDetail.image;
      this.detail.title = this.roomPromoDetail.promoName;
      this.detail.info1 = this.roomPromoDetail.validity;
      this.detail.info2 = this.roomPromoDetail.promoPeriod;
      this.detail.info3 = this.roomPromoDetail.minStay;
      this.detail.info4 = this.roomPromoDetail.minCount;
      this.detail.details = this.roomPromoDetail.details
      this.detail.titleText = "Room Promotion Detail"

    }

    if (this.roomPromoDetail != null){

      this.detail.image = this.roomPromoDetail.image;
      this.detail.title = this.roomPromoDetail.promoName;
      this.detail.info1 = this.roomPromoDetail.validity;
      this.detail.info2 = this.roomPromoDetail.promoPeriod;
      this.detail.info3 = this.roomPromoDetail.minStay;
      this.detail.info4 = this.roomPromoDetail.minCount;
      this.detail.details = this.roomPromoDetail.details
      this.detail.titleText = "Room Promotion Detail"

    }

    if (this.roomDetail != null){

      this.detail.image = this.roomDetail.image;
      this.detail.title = this.roomDetail.roomTitle;
      this.detail.details = this.roomDetail.detail;
      this.detail.titleText = "Room  Detail";

      var occupancy = parseInt(this.roomDetail.occupancy);

      if (occupancy > 0) {
        console.log(occupancy)
        this.detail.occupancy = new Array(occupancy)
      }

    }


  }



  dismissEvent(){
    this.view.dismiss()
  }

}
