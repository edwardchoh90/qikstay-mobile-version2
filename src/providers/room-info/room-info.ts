

/*
  Generated class for the RoomDetailsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export class RoomInfoProvider {

  roomDetailsJSON;
  roomDetails;

  constructor() {
    this.roomDetailsJSON =  '[{"image":"assets/img/roomDetails/Deluxe King_thumbnail.jpg","roomTitle":"Deluxe Room King Bed","size": "25sqm", "detail": "Slumber so quite and effortlessly in our Deluxe Rooms \' uber bedding an experience you will never forget", "occupancy":"1"},' +
      '{"image":"assets/img/roomDetails/Deluxe Twin_thumbnail.jpg","roomTitle":"Deluxe Room Queen Bed ", "size": "23sqm", "detail": "Nothing lulls one into slumber quite so effortlessly as our Deluxe Rooms \' uber beding experience. This room includes complimentary ", "occupancy":"2" },' +
      '{"image":"assets/img/roomDetails/Deluxe Twin_thumbnail.jpg","roomTitle":"Deluxe Room Double Queen Bed","size": "25sqm", "detail": "Nothing lulls one into slumber quite so effortlessly as our Deluxe Rooms \' uber beding experience. This room includes complimentary WIFI access, unlimited local calls and minibar (replenished once daily). There is no capacity for Extra Bed.","occupancy":"3" },' +
      '{"image":"assets/img/roomDetails/Deluxe King_thumbnail.jpg","roomTitle":"Executive Room Queen Bed","size":"26sqm", "detail": "Nothing lulls one into slumber quite so effortlessly as our Executive Rooms \' uber beding experience. This room includes complimentary WIFI access, unlimited local calls and minibar (replenished once daily). There is no capacity for Extra Bed." ,"occupancy":"2"},' +
      '{"image":"assets/img/roomDetails/SuiteRoundDesigner_thumbnail.jpg","roomTitle":"Suite Round Designer Bed", "size":"35sqm", "detail": "Sleek and expansive, our suites are ideal for discerning travellers or intimate special occasions. Sink into round designer beds and luxuriate in our plush bathtubs against the backdrop of a stunning view. Suite Room includes complimentary 1 way Limousine Airport Pickup, Buffet Breakfast, $50 F&amp;B Credit Daily, 4 pieces of laundry/Dry Clean daily per room(non-accumulative &amp; normal service), WIFI Access, Local Calls and Gourmet Bar (replenished once daily).","occupancy":"2" },' +
      '{"image":"assets/img/roomDetails/StandardRoomQueen_thumbnail.jpg","roomTitle":"Standard Room 2 single beds","size":"15sqm", "detail": "Our quaint and cosy Standard rooms provide comfort without excess, fusing quiet charm with all the modern conveniences. Room incudes complimentary WIFI access, unlimited local calls and minibar (replenished once daily). There is no capacity for Extra Bed. Request for bedding type is subject to availability","occupancy":"3" },' +
      '{"image":"assets/img/roomDetails/Deluxe Twin_thumbnail.jpg","roomTitle":"Deluxe Triple Room - 1 Queen and 1 Single Bed","size":"26sqm", "detail": "Our Triple Rooms are the perfect choice for families or friends travelling in groups. These rooms comes with 1 queen and 1 single bed, and are ideal for up to 3 persons.","occupancy":"3" },' +
      '{"image":"assets/img/roomDetails/DeluxeTripleRoom_thumbnail.jpg","roomTitle":"Deluxe Triple Room - 3 Single Bed","size":"26sqm", "detail": "Our Triple Rooms are the perfect choice for families or friends travelling in groups. These rooms comes with 3 single beds, and are ideal for up to 3 person.","occupancy":"3" }]'

    this.roomDetails = JSON.parse(this.roomDetailsJSON)
  }

}
