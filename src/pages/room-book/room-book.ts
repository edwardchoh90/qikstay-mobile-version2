import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LocalNotifications} from "@ionic-native/local-notifications";
import { DatePicker } from "@ionic-native/date-picker";
import Moment from 'moment'



/**
 * Generated class for the RoomBookPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-room-book',
  templateUrl: 'room-book.html',
})
export class RoomBookPage {

  showGuestDetail = false;
  notifications;
  checkInDate;
  checkOutDate;
  adultCount;
  childCount;
  roomCount;


  constructor(public navCtrl: NavController, public navParams: NavParams, public localNotification:LocalNotifications,
              public alertCtrl:AlertController, public datePicker: DatePicker) {
    this.checkInDate  = Moment();
    this.checkOutDate = Moment().add(1, 'days');
    this.adultCount = 1;
    this.childCount = 0;
    this.roomCount = 1;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RoomBookPage');
  }


  showDetails(){
    this.showGuestDetail = !this.showGuestDetail;
    console.log(this.showGuestDetail)
  }

  confirmBooking(){


    const alert = this.alertCtrl.create({
      title: 'Booking',
      subTitle: 'Confirm booking?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Login',
          handler: data => {

            let notification = {
              id:1,
              title:"BayView Hotel Booking",
              text:"Has just been confirmed",
              at: new Date(),
              every: 2
            }

            this.notifications.push(notification)

            this.localNotification.schedule(this.notifications);

          }
        }
      ]
    });
    alert.present();



  }

  datePick(dateWhen){

    if (dateWhen == 'start'){
      this.datePicker.show({
        date: new Date(),
        mode: 'date',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
        allowOldDates: false,
        minDate:Moment().toDate()

      }).then(
        date => {
          this.checkInDate = Moment(date)
        },
        err => {
          console.log('Error occurred while getting date: ', err)
        }
      );
    }

    if (dateWhen == 'end'){
      this.datePicker.show({
        date: new Date(),
        mode: 'date',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
        allowOldDates: false,
        minDate:Moment().toDate()

      }).then(
        date => {
          this.checkOutDate = Moment(date);
        },
        err => {
          console.log('Error occurred while getting date: ', err)
        }
      );
    }

  }

  amountAdjust(operator,occupant){

    //child

    if (operator == 'minus' && occupant == 'child'){
      var childCount = this.childCount -1 ;
      if (childCount <0){
        this.childCount == 0;
      } else {
      this.childCount = childCount;
      }
    }

    if (operator == 'plus' && occupant == 'child'){
      this.childCount = this.childCount + 1 ;
    }

    //adult

    if (operator == 'minus' && occupant == 'adult'){
      var adultCount = this.adultCount -1 ;
      if (adultCount <1){
        this.adultCount == 1;
      } else {
        this.adultCount = adultCount;
      }
    }

    if (operator == 'plus' && occupant == 'adult'){
      this.adultCount = this.adultCount + 1 ;
    }

    //room

    if (operator == 'minus' && occupant == 'room'){
      var roomCount = this.roomCount -1 ;
      if (roomCount <1){
        this.roomCount == 1;
      } else {
        this.roomCount = roomCount;
      }
    }

    if (operator == 'plus' && occupant == 'room'){
      this.roomCount = this.roomCount + 1 ;
    }





  }

  closeDetail(){
    this.showGuestDetail = false;
  }

}
