import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { HotelPromotionProvider } from "../../../providers/hotel-promotion/hotel-promotion";
import { ViewMoreComponent} from "../../../components/view-more/view-more";

/**
 * Generated class for the RoomPromotionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-room-promotion',
  templateUrl: 'room-promotion.html',
})
export class RoomPromotionPage {
  roomPromotionList
  constructor(public navCtrl: NavController, public navParams: NavParams, public promoProvider: HotelPromotionProvider,
              public modal: ModalController) {
    this.roomPromotionList = this.promoProvider.roomPromotions
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RoomPromotionPage');
  }

  roomPromotionDetail(roomPromo){
    this.modal.create(ViewMoreComponent,{roomPromoDetail:roomPromo}).present()
  }

}
