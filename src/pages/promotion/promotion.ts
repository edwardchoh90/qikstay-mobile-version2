import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { HotelPromotionProvider } from "../../providers/hotel-promotion/hotel-promotion";
import { FnbPromotionPage } from "./fnb-promotion/fnb-promotion";
import { RoomPromotionPage } from "./room-promotion/room-promotion";
import { ViewMoreComponent } from "../../components/view-more/view-more";

/**
 * Generated class for the PromotionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */



@Component({
  selector: 'page-promotion',
  templateUrl: 'promotion.html',
})
export class PromotionPage {

  foodPromotions;
  roomPromotions;




  tab2Root = FnbPromotionPage;
  tab3Root = RoomPromotionPage;


  constructor(public navCtrl: NavController, public navParams: NavParams,public modal: ModalController,
              public promoProvider: HotelPromotionProvider) {

  this.foodPromotions = this.promoProvider.foodPromotions
  this.roomPromotions = this.promoProvider.roomPromotions


  console.log(this.foodPromotions)

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromotionPage');
  }

  jumpTab(page){

    if (page =='food') {
      this.navCtrl.parent.select(1);
    }

    if (page == 'room'){
      this.navCtrl.parent.select(2);
    }

  }

  foodPromoDetail(foodPromo){
    console.log("clicked")
    this.modal.create(ViewMoreComponent,{fnbDetail:foodPromo}).present()
  }

  roomPromotionDetail(roomPromo){
    this.modal.create(ViewMoreComponent,{roomPromoDetail:roomPromo}).present()
  }

}
