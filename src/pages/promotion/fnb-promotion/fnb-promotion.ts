import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { HotelPromotionProvider } from "../../../providers/hotel-promotion/hotel-promotion";
import { ViewMoreComponent } from "../../../components/view-more/view-more";

/**
 * Generated class for the FnbPromotionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-fnb-promotion',
  templateUrl: 'fnb-promotion.html',
})
export class FnbPromotionPage {

  fnbPromotionList

  constructor(public navCtrl: NavController, public navParams: NavParams, public promoProvider: HotelPromotionProvider,
              public modal: ModalController) {
    this.fnbPromotionList = this.promoProvider.foodPromotions
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FnbPromotionPage');
  }

  foodPromoDetail(foodPromo){
    console.log("clicked")
    this.modal.create(ViewMoreComponent,{fnbDetail:foodPromo}).present()
  }

}
