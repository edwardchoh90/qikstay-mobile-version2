import { Component } from '@angular/core';
import {  NavController, NavParams, ModalController } from 'ionic-angular';
import { RoomInfoProvider } from "../../providers/room-info/room-info";
import { ViewMoreComponent } from "../../components/view-more/view-more";

/**
 * Generated class for the RoomDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-room-detail',
  templateUrl: 'room-detail.html',
})
export class RoomDetailPage {

  roomLists;

  constructor(public navCtrl: NavController, public navParams: NavParams, public roomInfo: RoomInfoProvider,
              public modal: ModalController) {
    this.roomLists = this.roomInfo.roomDetails
  }

  roomDetailView(roomDetail){
    this.modal.create(ViewMoreComponent,{roomDetail:roomDetail}).present()
  }

}
