import { Component } from '@angular/core';
import { ViewController,AlertController, LoadingController } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';
import Moment from 'moment'


@Component({
  selector:"search-event",
  template: `    

    

    <ion-content no-padding class="main-view">

      <div class="overlay" (click)="dismissSearch()"></div>
      
      <div class="modal_content">
        <ion-toolbar color="searchBarColor">
          <ion-title>Search Event</ion-title>
          <ion-buttons end>
            <button ion-button icon-only (click)="dismissSearch()">
              <ion-icon name="ios-close"></ion-icon>
            </button>
          </ion-buttons>
        </ion-toolbar>
      <ion-item>
        <ion-label stacked>Category</ion-label>
        <ion-input disabled="true"  [(ngModel)]="eventSelected" (click)="selectCategory()"></ion-input>
      </ion-item>
      <br>
      <ion-item>
        <ion-label stacked>Select Date</ion-label>
        <ion-input disabled="true" [(ngModel)]="dateSelected" (click)="datePickEvent()"></ion-input>
      </ion-item>
        <button color="searchBarColor"ion-button block (click)="search()"> Search </button>
        <button outline color="searchBarColor" ion-button block (click)="dismissSearch()"> Close </button>  
      </div>
    </ion-content>
    
  `,

})

export class SearchEvent {

  dateSelected;
  eventSelected = "All Category"
  eventType =["All Category","Concert & Tour Dates","Conferences and Tradeshows","Comedy", "Kids and Family", "Festival", "Film","Food & Wine",
              "Arts Galleries & Exhibit","Museums and Attractions", "Nightlife & Singles", "Outdoors and Recreations", "Performing Arts"]

  constructor(public view:ViewController, public datePicker:DatePicker, public altCtrl: AlertController, public loading:LoadingController) {

    this.dateSelected = Moment().format("DD/MM/YYYY")

  }

  dismissSearch(){
    this.view.dismiss();
  }

  datePickEvent(){

    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
      allowOldDates: false,

    }).then(
      date => {
        this.dateSelected = Moment(date).format("DD/MM/YYYY")
      },
      err => {
        console.log('Error occurred while getting date: ', err)
      }
    );

}

  selectCategory() {
    let selectEventType = this.altCtrl.create({
      title: "Event Category"
    })


    for (var i in this.eventType) {
      selectEventType.addInput({
        type: 'radio',
        label: this.eventType[i],
        value: this.eventType[i],
      })

    }

    selectEventType.addButton('Cancel');
    selectEventType.addButton({
      text: 'OK',
      handler: data => {
        this.eventSelected = data;

      }
    });

    selectEventType.present();

  }

  search(){
    let searchAnimation = this.loading.create({
      content:"Filtering",
      duration:1500,
    })

    searchAnimation.present()
    searchAnimation.onDidDismiss(data=>{
      this.view.dismiss();
    })
  }


}
