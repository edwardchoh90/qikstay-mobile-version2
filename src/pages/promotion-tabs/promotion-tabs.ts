import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PromotionPage } from "../promotion/promotion";
import { FnbPromotionPage } from "../promotion/fnb-promotion/fnb-promotion";
import { RoomPromotionPage } from "../promotion/room-promotion/room-promotion";

/**
 * Generated class for the PromotionTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-promotion-tabs',
  templateUrl: 'promotion-tabs.html',
})
export class PromotionTabsPage {
  tab1Root = PromotionPage;
  tab2Root = FnbPromotionPage;
  tab3Root = RoomPromotionPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromotionTabsPage');
  }

}
